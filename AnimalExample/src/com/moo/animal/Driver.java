package com.moo.animal;

public class Driver {
    public static void main(String[] args){

        Dog rover = new Dog("woof", "Kibble", "Lab");

        Dog spot = new Dog();

        Platypus perry =new Platypus("Chatter","Scientists", 3);

        spot.setBreed("Dalmation");
        spot.setFood("Canned Food");
        spot.setNoise("Bark");

        System.out.println(rover);
        System.out.println(spot);
        System.out.println(perry);


    }
}
