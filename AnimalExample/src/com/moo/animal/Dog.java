package com.moo.animal;

public class Dog extends Animal{

    private String breed;

    public Dog (){};

    public Dog (String noise, String meal, String breed) {

        super(noise, meal);
        this.breed= breed;
    } //Calls to Animal abstract class
    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String toString(){
        return "Dog: food= "+ this.getFood() + " noise="+ this.getNoise() + " breed= " + this.getBreed();
    }// End of Method Overriding toString
}
