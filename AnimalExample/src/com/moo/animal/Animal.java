package com.moo.animal;

public abstract class Animal {


    private String noise ;
    private String food;

 public Animal(){

 }

 //States our animal object and String arguments
 public Animal (String sound, String meal){

     this.noise = sound;
     this.food = meal;

 }

    public String getNoise() {
        return noise;
    }

    public void setNoise(String noise) {
        this.noise = noise;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }


}
