package com.moo.animal;

public class Platypus extends Animal {

//one comment here for the change!
    private int eggsLaid;

    public  Platypus () {}

    public Platypus (String noise, String food, int eggs){

        super(noise, food);
        this.eggsLaid = eggs;

    }
    public int getEggsLaid() {
        return eggsLaid;
    }

    public void setEggsLaid(int eggsLaid) {
        if(eggsLaid >= 0) {

            this.eggsLaid = eggsLaid;
        }
    }

    @Override
    public String toString(){
        return "Platypus: food= "+ this.getFood() + " noise="+ this.getNoise() + " eggs laid= " + this.getEggsLaid();
    }


}
